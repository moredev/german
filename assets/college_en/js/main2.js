(function($) {
    "use strict";
    var x = [$('.eqh-1'), $('.eqh-2'), $('.eqh-3'), $('.eqh-4')];
    var y = [$('.img-eqh-1'), $('.img-eqh-2'), $('.img-eqh-3'), $('.img-eqh-4')];
    var initmode;
    $(window).bind('load resize', function() {
        $.each(x, function() {
            $(this).css('height', 'auto');
            var i = 0;
            $.each($(this), function() {
                var h = $(this).outerHeight();
                if (i < h) {
                    i = h;
                } else {
                    i = i;
                }
            });
            $(this).css('height', i + 'px');
        });

        $.each(y, function() {
            $(this).css('height', 'auto');
            var j = 5000;
            $.each($(this), function() {
                var h = $(this).outerHeight();
                if (j > h) {
                    j = h;
                } else {
                    j = j;
                }
            });
            $(this).css('height', j + 'px');
        });
    });
    $('.topic_tab .nav-content').delegate('.tab-pane', 'change', function() {
        $.each(x, function() {
            $(this).css('height', 'auto');
            var i = 0;
            $.each($(this), function() {
                var h = $(this).outerHeight();
                if (i < h) {
                    i = h;
                } else {
                    i = i;
                }
            });
            $(this).css('height', i + 'px');
        });

        $.each(y, function() {
            $(this).css('height', 'auto');
            var j = 5000;
            $.each($(this), function() {
                var h = $(this).outerHeight();
                if (j > h) {
                    j = h;
                } else {
                    j = j;
                }
            });
            $(this).css('height', j + 'px');
        });
    });

    $(".top_bar").delegate("#top_more_link", "click", function(a) {
        return a.preventDefault(), a.stopPropagation(), $(this).next().is(":hidden") ? $(this).next().fadeIn() : $(this).next().fadeOut();
    });
    
    
    var tc = 6;
    var vc = 6;

    function tshow() {
        var i = 0;
        $("#topic .row > div").each(function() {
            if(i < tc) {
                $(this).css("display", "block");
            } else {
                $(this).css("display", "none");
            }
            i++;
        });
        if(tc >= i) {
            $("#topic .read_more").addClass("hidden")
        }
    }
    
    function vshow() {
        var i = 0;
        $("#video_zone .row > div").each(function() {
            if(i < vc) {
                $(this).css("display", "block");
            } else {
                $(this).css("display", "none");
            }
            i++;
        });
        if(vc >= i) {
            $("#video_zone .read_more").addClass("hidden")
        }
    }

    tshow();
    vshow();
        
    $("#topic .read_more").click(function() {
        tc += 6;
        tshow();
    });
    
    $("#video_zone .read_more").click(function() {
        vc += 6;
        vshow();
    });

    // nav
    // $(window).bind("load resize scroll", function() {
    //     var navtop = $(".nav_black").offset().top;
    //     var tbh = $(".top_bar").outerHeight() + 12;
    //     if ($(this).scrollTop() >= (navtop + 67)) {
    //         $("body").addClass("fixed_top");
    //     } else {
    //         $("body").removeClass("fixed_top");
    //     }
    // });

    // $('.side_menu').delegate('li.has_sub > a', 'click', function(e) {
    //     e.preventDefault();
    //     if ($(this).parent('li').hasClass('active')) {
    //         $(this).parent('li').removeClass('active').find('ul').slideUp();
    //     } else {
    //         $(this).parent('li').addClass('active').find('ul').slideDown().parent('li').siblings().removeClass('active').find('ul').slideUp();
    //     }
    // });
    $('.side_menu').delegate('li.has_sub > a', 'click', function(e) {
        e.preventDefault();
        if ($(this).parent('li').hasClass('active')) {
            $(this).parent('li').removeClass('active');
        } else {
            $(this).parent('li').addClass('active').siblings().removeClass('active');
        }
    });

    $('.totop').click(function() {
        var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
        $body.animate({
            scrollTop: $('header').offset().top
        }, 500);
        return false;
    });

    $('[data-toggle="dropdown"]').click(function(e) {
        if ($(window).width() < 753) {
            e.preventDefault();
        }
    });
    $('.dropdown-menu').click(function(e) {
        if ($(window).width() < 753) {
            e.stopPropagation();
        }
    });
    $('.dropdown-submenu').click(function(e) {
        if ($(window).width() < 753) {
            e.stopPropagation();
            e.preventDefault();
        }
    });
    $('.dropdown-submenu > a').click(function(e) {
        if ($(this).parent().hasClass('open')) {
            $(this).attr("aria-expanded", "false").parent().removeClass('open');
        } else {
            $(this).attr("aria-expanded", "true").parent().addClass('open');
        }
    });

})(jQuery);

// $(function() {
//     $.fn.extend({
//         treed: function(o) {

//             var openedClass = 'glyphicon-plus-sign';
//             var closedClass = 'glyphicon-minus-sign';

//             if (typeof o != 'undefined') {
//                 if (typeof o.openedClass != 'undefined') {
//                     openedClass = o.openedClass;
//                 }
//                 if (typeof o.closedClass != 'undefined') {
//                     closedClass = o.closedClass;
//                 }
//             };

//             var tree = $(this);
//             tree.addClass("tree");
//             tree.find('li').has("ul").each(function() {
//                 var branch = $(this); //li with children ul
//                 branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
//                 branch.addClass('branch');
//                 branch.on('click', function(e) {
//                     if (this == e.target) {
//                         var icon = $(this).children('i:first');
//                         icon.toggleClass(openedClass + " " + closedClass);
//                         $(this).children().children().toggle();
//                     }
//                 })
//             });

//             tree.find('.branch .indicator').each(function() {
//                 $(this).on('click', function() {
//                     $(this).closest('li').click();
//                 });
//             });

//             tree.find('.branch>button').each(function() {
//                 $(this).on('click', function(e) {
//                     $(this).closest('li').click();
//                     e.preventDefault();
//                 });
//             });
//         }
//     });

//     $('.tree').treed();
// });

// (function($) {
//     function findChild(parent, index) {
//         $(parent).children("a").prepend(index);
//         $(parent).children("ul").children('li').each(function(index1) {
//             index1 = index1 + 1;
//             var str = index + ' - ' + index1;
//             findChild(this, str);
//         });
//     }
//     $('ul.tree > li').each(function(index) {
//         findChild(this, index + 1);
//     });
// });
